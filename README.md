# How to run project

* Clone git repository into your local PC
* Setup `docker` and `docker-compose`
* Enter project's directory
* Run the command `docker-compose up -d`
* Run console command in the docker container  
  `docker exec -it optimacros_app bash -c "php bin/console tree:build storage/input.csv storage/output.json`

# How to run tests

* Repeat points **1-4** from previous block (if not yet)
* Run console command in the docker container  
  `docker exec -it optimacros_app bash -c "php vendor/bin/phpunit"`

FROM registry.gitlab.com/thewind1984/optimacros/backend:1.0.0

ARG USER=app

RUN useradd --create-home --uid 1000 --user-group --system ${USER}

COPY ./build/app/php.ini /usr/local/etc/php

WORKDIR /var/www/optimacros

RUN chown -R ${USER}: .

## Copy single files into container
COPY --chown=${USER}:${USER} composer.* symfony.lock .env .env.test phpunit.xml.dist ./

## Copy folders into container
## Cannot copy multiple folder within single command due to https://stackoverflow.com/a/30316600
COPY --chown=${USER}:${USER} bin bin
COPY --chown=${USER}:${USER} config config
COPY --chown=${USER}:${USER} public public
COPY --chown=${USER}:${USER} src src
COPY --chown=${USER}:${USER} tests tests

USER ${USER}

RUN composer install --no-interaction --no-ansi --optimize-autoloader --apcu-autoloader --prefer-dist \
    && rm -rf ~/.composer/cache/* \
    && php bin/console cache:clear

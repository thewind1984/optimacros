<?php

declare(strict_types=1);

use App\Service\FileReader\InputFileReader;
use App\Service\TreeBuilder\TreeBuilder;
use PHPUnit\Framework\TestCase;

class TreeBuilderTest extends TestCase
{
    protected TreeBuilder $treeBuilder;

    protected function setUp(): void
    {
        $this->treeBuilder = new TreeBuilder(new InputFileReader());
    }

    /**
     * @dataProvider getTrees
     */
    public function testSimpleTree(string $fileContent, string $jsonContent): void
    {
        $file = sys_get_temp_dir() . '/' . uniqid() . '.csv';
        file_put_contents($file,'Item Name,Type,Parent,Relation' . PHP_EOL . preg_replace('/^\s+/mui', '', $fileContent));
        $tree = $this->treeBuilder->build($file);
        $this->assertEquals($jsonContent, json_encode($tree->getJsonTree(), JSON_UNESCAPED_UNICODE));
    }

    public static function getTrees(): \Generator
    {
        yield ['
            "Total","Изделия и компоненты",""
            "ПВЛ","Изделия и компоненты","Total"
            "Стандарт.#1","Варианты комплектации","ПВЛ"
        ', '[{"itemName":"Total","parent":null,"children":[{"itemName":"ПВЛ","parent":"Total","children":[{"itemName":"Стандарт.#1","parent":"ПВЛ","children":[]}]}]}]'];

        yield ['
            Total,Изделия и компоненты,,
            ПВЛ,Изделия и компоненты,Total,
            Стандарт.#1,Варианты комплектации,ПВЛ,
            Тележка Б25.#2,Прямые компоненты,Стандарт.#1,Тележка Б25
            Тележка Б25,Изделия и компоненты,Total,
            Стандарт.#5,Варианты комплектации,Тележка Б25,
            РБ ЦДЛР.9855.00.02.000.#17,Прямые компоненты,Стандарт.#5,РБ ЦДЛР.9855.00.02.000
        ', '[{"itemName":"Total","parent":null,"children":[{"itemName":"ПВЛ","parent":"Total","children":[{"itemName":"Стандарт.#1","parent":"ПВЛ","children":[{"itemName":"Тележка Б25.#2","parent":"Стандарт.#1","children":[{"itemName":"Стандарт.#5","parent":"Тележка Б25.#2","children":[{"itemName":"РБ ЦДЛР.9855.00.02.000.#17","parent":"Стандарт.#5","children":[]}]}]}]}]},{"itemName":"Тележка Б25","parent":"Total","children":[{"itemName":"Стандарт.#5","parent":"Тележка Б25","children":[{"itemName":"РБ ЦДЛР.9855.00.02.000.#17","parent":"Стандарт.#5","children":[]}]}]}]}]'];
    }
}

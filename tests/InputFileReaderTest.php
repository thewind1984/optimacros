<?php

declare(strict_types=1);

use App\Service\FileReader\FileReader;
use App\Service\FileReader\InputFileNotFound;
use App\Service\FileReader\InputFileReader;
use PHPUnit\Framework\TestCase;

final class InputFileReaderTest extends TestCase
{
    protected FileReader $fileReader;

    protected function setUp(): void
    {
        $this->fileReader = new InputFileReader();
    }

    public function testMissingRemoteFile(): void
    {
        $this->expectException(InputFileNotFound::class);
        $resource = $this->fileReader->open('https://example.com/test.csv');
    }

    public function testLockedLocalFile(): void
    {
        $file = sys_get_temp_dir() . '/' . uniqid() . '.csv';
        file_put_contents($file, '');

        $resource = $this->fileReader->open($file);
        $this->assertNotEquals(false, $resource);
    }
}

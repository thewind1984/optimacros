<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\TreeBuilder\TreeBuilder;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;

#[AsCommand(name: 'tree:build')]
final class TreeBuildCommand extends Command
{
    public function __construct(
        private readonly TreeBuilder $treeBuilder,
        private readonly KernelInterface $kernel
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('input', InputArgument::REQUIRED, 'Input file path (could be and URL)')
            ->addArgument('output', InputArgument::REQUIRED, 'Output file name')
        ;

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $startTime = microtime(true);

        try {
            $tree = $this->treeBuilder->build($input->getArgument('input'));

            $output->writeln(sprintf('Tree was built with nodes count [%d]', $tree->getNodesCount()));

            file_put_contents(
                $this->kernel->getProjectDir() . '/' . ltrim($input->getArgument('output'), '/'),
                json_encode($tree->getJsonTree(), JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT)
            );

            return Command::SUCCESS;
        } catch (\Throwable $e) {
            $output->writeln(vsprintf('Exception [%s] says: %s in [%s:%d]', [
                get_class($e),
                $e->getMessage(),
                $e->getFile(),
                $e->getLine(),
            ]));

            return Command::FAILURE;
        } finally {
            $this->treeBuilder->close();

            $output->writeln(vsprintf('Command finished after [%s sec] with total memory [%s bytes]', [
                sprintf('%.04f', microtime(true) - $startTime),
                memory_get_usage(true),
            ]));
        }
    }
}

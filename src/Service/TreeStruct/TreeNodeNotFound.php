<?php

declare(strict_types=1);

namespace App\Service\TreeStruct;

final class TreeNodeNotFound extends \Exception
{}

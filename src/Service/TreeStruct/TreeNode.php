<?php

declare(strict_types=1);

namespace App\Service\TreeStruct;

final class TreeNode
{
    private ?TreeNode $parent = null;

    /** @var TreeNode[] */
    private array $children = [];

    public function __construct(
        public readonly string $name,
        public readonly string $id
    ) {}

    public function setParent(?TreeNode $treeNode): void
    {
        $this->parent = $treeNode;
    }

    public function addChild(TreeNode $child): void
    {
        $this->children[] = $child;
    }

    public function getParent(): ?TreeNode
    {
        return $this->parent;
    }

    public function hasChildren(): bool
    {
        return count($this->children) > 0;
    }

    public function getChildren(): array
    {
        return $this->children;
    }
}

<?php

declare(strict_types=1);

namespace App\Service\TreeStruct;

final class Tree
{
    /** @var TreeNode[] */
    private array $nodes = [];

    /**
     * @throws TreeNodeNotFound
     */
    public function getNode(string $id): TreeNode
    {
        if (!isset($this->nodes[$id])) {
            throw new TreeNodeNotFound(sprintf('TreeNode with id [%s] not found', $id));
        }

        return $this->nodes[$id];
    }

    public function addNode(TreeNode $node, string $id): void
    {
        $this->nodes[$id] = $node;
    }

    public function getNodesCount(): int
    {
        return count($this->nodes);
    }

    /**
     * @param TreeNode[]|null $nodes
     */
    public function getJsonTree(?array $nodes = null, ?string $parentName = null): array
    {
        $data = [];

        $isRoot = $nodes === null;

        foreach ($nodes ?? $this->nodes as $node) {
            if ($isRoot && $node->getParent() !== null) {
                continue;
            }

            $data[] = [
                'itemName' => $node->name,
                'parent' => $parentName,
                'children' => $node->hasChildren() ? $this->getJsonTree($node->getChildren(), $node->name) : [],
            ];
        }

        return $data;
    }
}

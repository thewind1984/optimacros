<?php

declare(strict_types=1);

namespace App\Service\FileReader;

final class InputFileReader implements FileReader
{
    public function open(string $file): mixed
    {
        if (preg_match('#^https?://#ui', $file) === 1) {
            $localFile = sys_get_temp_dir() . '/' . uniqid() . '.csv';
            copy($file, $localFile);
        }

        $inputFile = $localFile ?? $file;
        if (!file_exists($inputFile)) {
            throw new InputFileNotFound(sprintf('File [%s] not found', $inputFile));
        }

        if (($resource = fopen($inputFile, 'r')) === false) {
            throw new InputFileReadError(sprintf('Cannot read file [%s]', $inputFile));
        }

        return $resource;
    }
}

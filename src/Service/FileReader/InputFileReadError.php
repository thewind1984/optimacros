<?php

declare(strict_types=1);

namespace App\Service\FileReader;

final class InputFileReadError extends \Exception
{}

<?php

declare(strict_types=1);

namespace App\Service\FileReader;

interface FileReader
{
    /**
     * @return resource
     * @throws InputFileNotFound
     * @throws InputFileReadError
     */
    public function open(string $file): mixed;
}

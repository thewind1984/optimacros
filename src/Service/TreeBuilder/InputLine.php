<?php

declare(strict_types=1);

namespace App\Service\TreeBuilder;

final class InputLine
{
    public readonly ItemType $itemType;
    public readonly ?string $relationName;
    public readonly ?string $parentItemName;

    public function __construct(
        public readonly string $itemName,
        string $itemType,
        string $parentItemName,
        ?string $relationName = null
    ) {
        $this->itemType = ItemType::from($itemType);
        $this->parentItemName = ($parent = trim($parentItemName)) !== '' ? $parent : null;
        $this->relationName = $relationName !== null && ($relation = trim($relationName)) !== '' ? $relation : null;
    }

    public function getKey(FieldType $fieldType): ?string
    {
        $field = match ($fieldType) {
            FieldType::Name => $this->itemName,
            FieldType::Parent => $this->parentItemName,
            FieldType::Relation => $this->relationName
        };

        if ($field === null) {
            return null;
        }

        return md5($field);
    }
}

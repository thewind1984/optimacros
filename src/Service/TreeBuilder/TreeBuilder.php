<?php

declare(strict_types=1);

namespace App\Service\TreeBuilder;

use App\Service\FileReader\FileReader;
use App\Service\FileReader\InputFileNotFound;
use App\Service\FileReader\InputFileReadError;
use App\Service\TreeStruct\Tree;
use App\Service\TreeStruct\TreeNode;
use App\Service\TreeStruct\TreeNodeNotFound;

final class TreeBuilder
{
    private mixed $resource = null;

    public function __construct(
        private readonly FileReader $fileReader
    ) {}

    /**
     * @throws InputFileNotFound
     * @throws InputFileReadError
     * @throws TreeNodeNotFound
     */
    public function build(
        string $inputFile
    ): Tree {
        $this->resource = $this->fileReader->open($inputFile);

        $headerLinePassed = 0;
        $separator = '.';
        $tree = new Tree();
        $relationsToParents = [];
        $parentsToItems = [];

        while (($line = fgetcsv($this->resource, 1000, $separator)) !== false) {
            if ($headerLinePassed++ < 1) {
                // detect actual separator of csv file
                preg_match('/^.*?(?<s>[,;])/ui', $line[0], $matches);
                $separator = $matches['s'] ?? ',';

                continue;
            }

            $item = new InputLine(...$line);

            $treeParent = $this->getOrCreateItem($tree, $item->parentItemName, $parentKey = $item->getKey(FieldType::Parent));
            $treeItem = $this->getOrCreateItem($tree, $item->itemName, $item->getKey(FieldType::Name));

            if ($item->itemType !== ItemType::Straight) {
                if (!isset($parentsToItems[$parentKey])) {
                    $parentsToItems[$parentKey] = [];
                }
                $parentsToItems[$parentKey][] = $treeItem->id;
            }

            $treeItem->setParent($treeParent);
            $treeParent?->addChild($treeItem);

            if ($item->itemType === ItemType::Straight && $item->relationName !== null) {
                $relationKey = $item->getKey(FieldType::Relation);
                if (!isset($relationsToParents[$relationKey])) {
                    $relationsToParents[$relationKey] = [];
                }
                $relationsToParents[$relationKey][] = $treeItem->id;
            }
        }

        $this->insertRelationSubTrees($tree, $relationsToParents, $parentsToItems);

        return $tree;
    }

    public function close(): void
    {
        if (is_resource($this->resource)) {
            fclose($this->resource);
        }
    }

    private function getOrCreateItem(Tree $tree, ?string $nodeName, ?string $id): ?TreeNode
    {
        if ($id === null) {
            return null;
        }

        try {
            $node = $tree->getNode($id);
        } catch (TreeNodeNotFound $e) {
            $node = new TreeNode($nodeName, $id);
            $tree->addNode($node, $id);
        }

        return $node;
    }

    /**
     * @throws TreeNodeNotFound
     */
    private function insertRelationSubTrees(Tree $tree, array $relationsToParents, array $parentsToItems): void
    {
        $relationsToParents = array_intersect_key($relationsToParents, $parentsToItems);

        foreach ($relationsToParents as $relationKey => $treeItemIds) {
            foreach ($treeItemIds as $treeItemId) {
                foreach ($parentsToItems[$relationKey] as $parentsToItem) {
                    $tree->getNode($treeItemId)->addChild($tree->getNode($parentsToItem));
                }
            }
        }
    }
}

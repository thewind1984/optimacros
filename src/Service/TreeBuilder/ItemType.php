<?php

declare(strict_types=1);

namespace App\Service\TreeBuilder;

enum ItemType: string
{
    case Items = 'Изделия и компоненты';
    case Cases = 'Варианты комплектации';
    case Straight = 'Прямые компоненты';
}

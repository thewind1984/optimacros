<?php

declare(strict_types=1);

namespace App\Service\TreeBuilder;

enum FieldType
{
    case Name;
    case Parent;
    case Relation;
}
